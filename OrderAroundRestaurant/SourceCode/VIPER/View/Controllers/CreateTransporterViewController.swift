//
//  CreateTransporterViewController.swift
//  OrderAroundRestaurant
//
//  Created by Prem's on 23/11/20.
//  Copyright © 2020 CSS. All rights reserved.
//

import UIKit
import GooglePlaces
import ObjectMapper

class CreateTransporterViewController: BaseViewController {

    @IBOutlet var contentScrollView: UIScrollView!
    @IBOutlet var addPhotoBtn: UIButton!
    @IBOutlet var nameTxtFld: UITextField!
    @IBOutlet var emailTxtFld: UITextField!
    @IBOutlet var phoneNoTxtFld: UITextField!
    
    @IBOutlet var phoneNumberView: UIView!
    @IBOutlet var countryCodeImgView: UIImageView!
    @IBOutlet var countryCodeLbl: UILabel!
    @IBOutlet var passwordTxtFld: UITextField!
    @IBOutlet var addressView: UIView!
    @IBOutlet var addressLbl: UILabel!
    
    var placesHelper : GooglePlacesHelper?
    var latStr: String?
    var longStr: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationcontroller()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupUI(){
        
        contentScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        hideKeyboardWhenTappedAround()
        countryCodeImgView.image = UIImage(named: "CountryPicker.bundle/AU")
        countryCodeLbl.text = "+61"
        addShadowTextField(textField: nameTxtFld)
        addShadowTextField(textField: emailTxtFld)
        addShadowView(view: phoneNumberView)
        addShadowTextField(textField: passwordTxtFld)
        addShadowView(view: addressView)
        if self.placesHelper == nil {
               self.placesHelper = GooglePlacesHelper()
        }
    }
    
    private func setNavigationcontroller(){
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.primary
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont.bold(size: 18), NSAttributedString.Key.foregroundColor : UIColor.white]
        
        let btnBack = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back-white"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnBack.addTarget(self, action: #selector(self.onbackAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButtonItems([item], animated: true)
        self.title = "Add Delivery People"
    }
    
    @objc func onbackAction(){
        
        self.navigationController?.popViewController(animated: true)
    }
        
    @IBAction func countryCodeBtnAction(_ sender: Any) {
        
        let countryCodeController = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.CountryCodeViewController) as! CountryCodeViewController
        countryCodeController.delegate = self
        self.navigationController?.pushViewController(countryCodeController, animated: true)
    }
    
    @IBAction func addressBtnAction(_ sender: Any) {
        self.view.endEditing(true)
        placesHelper?.getGoogleAutoComplete { (place) in
            self.addressLbl.text = "\(place.formattedAddress ?? "")"
            self.latStr = String(format: "%.8f", place.coordinate.latitude)
            self.longStr = String(format: "%.8f", place.coordinate.longitude)
            
        }
    }
    
    @IBAction func addPhotoBtnAction(_ sender: Any) {
        
    }
    
    @IBAction func createBtnAction(_ sender: Any) {
        
        validateParams()
    }
    
    func addTransporterAPI() {
        self.showActivityIndicator()
        var params = [String: Any]()
        params["name"] = nameTxtFld.text ?? ""
        params["email"] = emailTxtFld.text ?? ""
        params["country_code"] = countryCodeLbl.text ?? ""
        params["phone"] = phoneNoTxtFld.text ?? ""
        params["password"] = passwordTxtFld.text ?? ""
        params["address"] = addressLbl.text ?? ""
        params["latitude"] = self.latStr ?? ""
        params["longitude"] = self.longStr ?? ""
        self.presenter?.IMAGEPOST(api:  Base.addTransporter.rawValue, params: params, methodType: .POST, imgData: [:], imgName: "image", modelClass: Transporter.self, token: true)
    }
    
    func validateParams() {
        
        if nameTxtFld.text?.count == 0{
            
            showToast(msg: "Please Enter Name")
            return
        }
        
        if emailTxtFld.text?.count == 0{
            
            showToast(msg: "Please Enter Email")
            return
        }
        
        guard isValid(email: emailTxtFld.text ?? "") else{
            showToast(msg: "Please Enter A Valid Email")
            return
        }
        
        if phoneNoTxtFld.text?.count ?? 0 == 0{
            
            showToast(msg: "Please Enter Phone Number")
            return
        }
        
        if passwordTxtFld.text?.count == 0{
            
            showToast(msg: "Please Enter Password")
            return
        }
        
        if ((passwordTxtFld.text?.isValidPassword()) != nil) == false{
         
            showToast(msg: "Please Enter A Valid Password")
            return
        }
        
        if addressLbl.text?.count ?? 0 == 0{
            
            showToast(msg: "Please Select Address")
            return
        }
        
        addTransporterAPI()
    }
}

extension CreateTransporterViewController: CountryCodeViewControllerDelegate{
    
    func fetchCountryCode(Value: Country) {
        
        countryCodeImgView.image = UIImage(named: "CountryPicker.bundle/"+Value.code)
        countryCodeLbl.text = Value.dial_code
    }
}

extension CreateTransporterViewController: UITextFieldDelegate{
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
//        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
//            nextField.becomeFirstResponder()
//        }else{
//            textField.resignFirstResponder()
//        }
//        return true
//    }
}

//MARK: VIPER Extension:
extension CreateTransporterViewController: PresenterOutputProtocol {
    func showSuccess(dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
        self.HideActivityIndicator()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func showError(error: CustomError) {
        print(error)
        let alert = showAlert(message: error.localizedDescription)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: {
                self.HideActivityIndicator()
            })
        }
    }
}
