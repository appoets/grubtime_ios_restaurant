//
//  DeliveryPeopleViewController.swift
//  OrderAroundRestaurant
//
//  Created by Prem's on 23/11/20.
//  Copyright © 2020 CSS. All rights reserved.
//

import UIKit
import ObjectMapper

class DeliveryPeopleViewController: BaseViewController {

    @IBOutlet var peopleTableView: UITableView!
    var deliveryPeopleArray: [DeliveryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationcontroller()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getTransportList()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func getTransportList(){
        showActivityIndicator()
        self.presenter?.GETPOST(api: Base.getTransportList.rawValue, params: [:], methodType: .GET, modelClass: DeliveryModel.self, token: true)
        
    }
    
    private func setupUI() {
    
        peopleTableView.tableFooterView = UIView()
        let historyCellNib = UINib(nibName: XIB.Names.HistoryTableViewCell, bundle: nil)
        peopleTableView.register(historyCellNib, forCellReuseIdentifier: XIB.Names.HistoryTableViewCell)
    }
    
    private func setNavigationcontroller(){
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.primary
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont.bold(size: 18), NSAttributedString.Key.foregroundColor : UIColor.white]
        
        let btnBack = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back-white"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnBack.addTarget(self, action: #selector(self.onbackAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButtonItems([item], animated: true)
        self.title = "Delivery People"
        
        let btnFilter = UIButton(type: .custom)
        //btnFilter.setImage(UIImage(named: "plus"), for: .normal)
        btnFilter.setTitle("ADD", for: .normal)
        btnFilter.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        btnFilter.addTarget(self, action: #selector(self.addTransporterAction), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btnFilter)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
    }
        
    @objc func onbackAction(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addTransporterAction(){
        let createTranspoterVC = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.CreateTransporterViewController) as! CreateTransporterViewController
        self.navigationController?.pushViewController(createTranspoterVC, animated: true)
    }

}

extension DeliveryPeopleViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return deliveryPeopleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let historyCell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.HistoryTableViewCell) as! HistoryTableViewCell
        historyCell.updateDeliveryPeopleCell(deliveryObj: deliveryPeopleArray[indexPath.row])
        return historyCell
    }
}

/******************************************************************/
//MARK: VIPER Extension:
extension DeliveryPeopleViewController: PresenterOutputProtocol {
    func showSuccess(dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
        if String(describing: modelClass) == model.type.DeliveryModel {
            HideActivityIndicator()
            self.deliveryPeopleArray = dataArray as! [DeliveryModel]
            self.peopleTableView.reloadData()
        }
    }
    
    func showError(error: CustomError) {
        print(error)
        let alert = showAlert(message: error.localizedDescription)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: {
                self.HideActivityIndicator()
            })
        }
    }
}
/******************************************************************/
