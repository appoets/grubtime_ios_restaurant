//
//  PaymentDetailViewController.swift
//  OrderAroundRestaurant
//
//  Created by Prem's on 26/11/20.
//  Copyright © 2020 CSS. All rights reserved.
//

import UIKit

class PaymentDetailViewController: UIViewController {

    @IBOutlet var detailBtn: UIButton!
    @IBOutlet var detailLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        detailLbl.text = "Click here to see your bank details"
        detailBtn.layer.cornerRadius = 5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationcontroller()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func setNavigationcontroller(){
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.primary
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont.bold(size: 18), NSAttributedString.Key.foregroundColor : UIColor.white]
        
        let btnBack = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back-white"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnBack.addTarget(self, action: #selector(self.onbackAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButtonItems([item], animated: true)
        self.title = "Bank Details"
    }

    @objc func onbackAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func detailBtnAction(_ sender: Any) {
        if let url = URL(string: "https://dashboard.stripe.com"){
            UIApplication.shared.open(url, options: [:]) { (success) in
            }
        }
    }
}
