//
//  ProfileEntity.swift
//  OrderAroundRestaurant
//
//  Created by CSS on 14/03/19.
//  Copyright © 2019 CSS. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProfileModel : Mappable {
    
    var id : Int?
    var name : String?
    var email : String?
    var phone : String?
    var avatar : String?
    var default_banner : String?
    var description : String?
    var offer_min_amount : Int?
    var offer_percent : Int?
    var offer_type: String?
    var estimated_delivery_time : Int?
    var address : String?
    var maps_address : String?
    var latitude : Double?
    var longitude : Double?
    var pure_veg : Int?
    var rating : Int?
    var bank : BankDestails?
    var rating_status : Int?
    var status : String?
    var device_token : String?
    var device_id : String?
    var device_type : String?
    var created_at : String?
    var updated_at : String?
    var deleted_at : String?
    var currency : String?
    var cuisines : [Cuisines]?
    var timings : [Timings]?
    var tokens : [Tokens]?
    var deliveryoption : [DeliveyOptions]?
    var halal : Int?
    var free_delivery : Int?
    var image_banner_id : String?
    var otp : Int?
    var training_module : [FoodSafetyModel]?
    var party_catering: Int?
    var meal_service: Int?
    var stripe_cust_id: String?
    var stripe_connect_url: String?
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
        avatar <- map["avatar"]
        default_banner <- map["default_banner"]
        description <- map["description"]
        offer_min_amount <- map["offer_min_amount"]
        offer_percent <- map["offer_percent"]
        offer_type <- map["offer_type"]
        estimated_delivery_time <- map["estimated_delivery_time"]
        address <- map["address"]
        maps_address <- map["maps_address"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        pure_veg <- map["pure_veg"]
        rating <- map["rating"]
        rating_status <- map["rating_status"]
        status <- map["status"]
        device_token <- map["device_token"]
        device_id <- map["device_id"]
        device_type <- map["device_type"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        deleted_at <- map["deleted_at"]
        currency <- map["currency"]
        cuisines <- map["cuisines"]
        timings <- map["timings"]
        tokens <- map["tokens"]
        deliveryoption <- map["deliveryoption"]
        bank <- map["bank"]
        halal <- map["halal"]
        free_delivery <- map["free_delivery"]
        image_banner_id <- map["image_banner_id"]
        training_module <- map["training_module"]
        otp  <- map["otp"]
        party_catering <- map["party_catering"]
        meal_service <- map["meal_service"]
        stripe_cust_id <- map["stripe_cust_id"]
        stripe_connect_url <- map["stripe_connect_url"]
    }
    
}

struct Tokens : Mappable {
    var id : String?
    var user_id : Int?
    var client_id : Int?
    var name : String?
    var scopes : [String]?
    var revoked : Bool?
    var created_at : String?
    var updated_at : String?
    var expires_at : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        user_id <- map["user_id"]
        client_id <- map["client_id"]
        name <- map["name"]
        scopes <- map["scopes"]
        revoked <- map["revoked"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        expires_at <- map["expires_at"]
    }
}

struct BankDestails : Mappable {
    
    
    var id: Int?
    var shop_id : Int?
    var bank_id : String?
    var bank_name : String?
    var account_number : String?
    var holder_name : String?
    var routing_number : String?
   
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        shop_id <- map["shop_id"]
        bank_id <- map["bank_id"]
        bank_name <- map["bank_name"]
        account_number <- map["account_number"]
        holder_name <- map["holder_name"]
        routing_number <- map["routing_number"]
        
    }
    
  
    
    
}

struct DeliveyOptions : Mappable {
    
    
    var id : Int?
    var shop_id : Int?
    var name : String?
    var delivery_option_id : Int?
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        id <- map["id"]
        shop_id <- map["shop_id"]
        name <- map["name"]
        delivery_option_id <- map["delivery_option_id"]
    }
}


struct Timings : Mappable {
    
    var id : Int?
    var shop_id : Int?
    var start_time : String?
    var end_time : String?
    var day : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        shop_id <- map["shop_id"]
        start_time <- map["start_time"]
        end_time <- map["end_time"]
        day <- map["day"]
    }
    
}


struct FoodSafetyModel : Mappable {
    

    var name : String?
    var url : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        
        name <- map["name"]
        url <- map["url"]
        
    }
    
}


struct OTPResponseModel : Mappable {
    
    var message : String?
    var user : ProfileModel?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        message <- map["message"]
        user <- map["user"]
        
    }
}
